#!/usr/bin/python

import socket
from thread import *

host = "127.0.0.1"
port = 8082

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

print("socket is created")

s.bind((host, port))

print("socket has been bounded")

s.listen(10)

print("socket is ready")

def Mythread(conn):
		msg = ("thanks for connecting to server")
		conn.send(msg.encode())

		while True:
			data = conn.recv(1024)
			reply = "OK." + data.decode()
			if not data:
				break;
			print(reply)
			conn.sendall(data.encode())

		conn.close()

while 1:
	conn, addr = s.accept()
	print("Connected with" + addr[0] + "!" + str(addr[1]))
	start_new_thread(Mythread, (conn,))

s.close()